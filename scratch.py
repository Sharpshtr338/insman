import pyodbc

def subCheckDrivers():
    return pyodbc.drivers()

def subCreateDBConnection(bolTest):
    if bolTest == True:
        server = 'localhost'
        database = 'InsMan'
        oSql = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+'; Trusted_Connection=Yes;DATABASE='+database)
    else:
        oSql = pyodbc.connect('driver={SQLOLEDB};server=+ServerName+; database = +MSQLDatabase +; trusted_connection = true')

    return oSql

def subGetNewRowsFromDB(oSql):
    cursor = oSql.cursor()
    cursor.execute('select * from [dbo].[TactonTable] where [Processed] is null')
    rows = list(cursor.fetchall())
    return rows

drivers = subCheckDrivers
print(drivers())
oSql = subCreateDBConnection(True)
lstNewRows = subGetNewRowsFromDB(oSql)
print(lstNewRows)

qry = '''SELECT
    a.Sol_Brand,
	a.Sol_Model,
	a.Sol_PortSize,
	a.Sol_PortSizeFloat,
	a.[3DModelName],
    b.Sol_Model,
	b.SchemCode
    FROM [dbo].[TWOSolenoidSelection] a
    left outer JOIN [dbo].[TactonTable] b
    ON b.Sol_Model LIKE '%' + a.Sol_Model + '%'''